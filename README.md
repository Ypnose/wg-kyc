kwyc - (m)[k]sh [w]ireguard [y]pnose's [c]lient
===============================================

A script to initiate WireGuard VPN tunnel.

## Why?

`wg-quick(8)` is written in `bash(1)`. Even if it works flawlessly, I
would like to avoid this requirement on my systems. `kwyc` uses
`mksh(1)`. My goal is to add it in solyste.

## Usage

	$ kwyc wg0.conf
	[kwyc] Created wg0 interface
	[kwyc] Added 192.168.1.10/24 address to wg0
	[kwyc] Added 192.168.1.1 nameserver to /etc/resolv.conf
	[kwyc] Default route updated
	[kwyc] Firewall rule added for 9999 (OUTPUT)
	[kwyc] Current IP: 140.82.121.3
	[kwyc] kwyc PID: 20961

Once the script is launched, it will wait for `SIGINT` or `SIGKILL`
signals. `kwyc` displays its `PID`. Once it is triggered, the
interface will be destroyed and the settings cleaned.

It is possible to verify the settings used in `kwyc` by using `-c`
option, as the last parameter :

	$ kwyc wg0.conf -c
	[Interface]
	PrivateKey = 0R23vwz6q4BlNnmwFx7Xoo4AbFg7Kh0CsEsw50J0/zU=
	[Peer]
	PublicKey = nVHrwwuQl0iIjH+Gyv5bfSJPMhwwogBJTnjTQrueP3g=
	PresharedKey = 3GLbvKNgvYL7PjPfy6VwtHeJaJE8OKYtydsG91Ivl8o=
	AllowedIPs = 0.0.0.0/0
	Endpoint = myserver.rocks:51820
	[Network]
	IP = 10.200.150.8/24
	DNS = 10.200.150.1
	PORT = 51820

`[Network]` section includes the settings **only** used by `kwyc`.

## Notes

To make sure regressions are not introduced in the config parser, tests
were written. Go in `tests/` directory and launch `./run`.

For more information about WireGuard and `wg-quick(8)`, visit the
official website :

https://www.wireguard.com/  
https://git.zx2c4.com/wireguard-tools/about/src/man/wg-quick.8

## Author

Ypnose - http://ywstd.fr/

## License

BSD 3-Clause License. Check LICENSE.
